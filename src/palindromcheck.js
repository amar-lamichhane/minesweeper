function checkPalindrom (str) {
  let rotate = '';
  for (let i = str.length - 1; i >= 0; i -- ) {
    rotate += str[i];
  }
  return rotate === str;
}


function cutPalindrom (str) {
  let checkStr = '';
  let longestPalindrom = "";
  for (let i = 0; i < str.length; i ++) {
    checkStr += str[i];
    if (checkStr.length > 1) {
      if (checkPalindrom(checkStr)) {
        if (checkStr.length > longestPalindrom.length) {
          longestPalindrom = checkStr;
        }

        if ((i+1) < str.length) {

          if (!checkPalindrom(checkStr + str[i+1])){
            checkStr = "";
            longestPalindrom = "";
          }

        } else {
          checkStr = "";
          longestPalindrom = "";
        }

      }
    }
    console.log(checkStr, str);
  }
  return checkStr;
}

console.log(cutPalindrom('aaacodocdedeehangeregnah'));
