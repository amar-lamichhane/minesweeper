export interface TradeInterface {
  buyAmount: number;
  sellAmount: number;
  rate: number;
}
