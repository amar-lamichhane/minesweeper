import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {TradeInterface} from '../../interfaces/trade.interface';
import {BuyConfirmationService} from '../../buy-confirmation.service';
import {Subscription} from 'rxjs/Subscription';
import {DecimalPipe} from '@angular/common';

@Component({
  selector: 'app-buy-confirmation-dialog',
  templateUrl: 'buy-confirmation.component.html',
   styleUrls: ['buy-confirmation.component.css'],
})
export class BuyConfirmationComponent implements OnDestroy, OnInit {
  protected buySubscription: Subscription;
  public dataRequestInProgress = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: TradeInterface,
              protected dialogRef: MatDialogRef<any>,
              protected snackbar: MatSnackBar,
              protected buyService: BuyConfirmationService) {
  }

  public ngOnInit(): void {
    const pipe = new DecimalPipe('en-US');
    this.data.buyAmount = pipe.transform(this.data.buyAmount) as any;
    this.data.sellAmount = pipe.transform(this.data.sellAmount) as any;
  }

  /**
   * Confirm the trade
   */
  public confirm(): void {
    this.dataRequestInProgress = true;
    this.buyService.buy(this.data).subscribe(() => {
      this.dataRequestInProgress = false;
      this.snackbar.open(`You bought £${this.data.buyAmount} for €${this.data.sellAmount}`,
        null, {verticalPosition: 'top', duration: 3000});
      this.dialogRef.close(true);
    }, () => {
      this.dataRequestInProgress = false;
      this.dialogRef.close(false);
    });
  }

  /**
   * Cancel the trade
   */
  public cancel(): void {
    this.dialogRef.close(false);
  }
  public ngOnDestroy(): void {
    if (this.buySubscription) {
      this.buySubscription.unsubscribe();
    }
  }

}
