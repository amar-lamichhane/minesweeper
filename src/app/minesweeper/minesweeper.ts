export class Subscription {

  public constructor(private observable: Observable<any>, private success: any, private error: any) {
  }

  public unsubscribe(): void {
    const observers = this.observable.getObservers();
    for (let index = 0; index <  observers.successes.length; index ++) {
      if (observers.successes[index] === this.success) {
        observers.successes.splice(index, 1);
      }
    }

    for (let index = 0; index <  observers.errors.length; index ++) {
      if (observers.errors[index] === this.error) {
        observers.errors.splice(index, 1);
      }
    }

  }
}

export class Observable<T> {
  private success: Array<(resp: T) => void> = [];
  protected error: Array<(resp: any) => void> = [];

  public subscribe(success: (resp: T) => void, error?: (e: any) => {}): Subscription {
    this.success.push(success);
    if (error) {
      this.error.push(error);
    }
    return new Subscription(this, success, error);
  }
  private callSuccess(object: T): void {
    for (let i = 0; i < this.success.length; i ++) {
      this.success[i](object);
    }
  }

  private callError(object: any): void {
    for (let i = 0; i < this.success.length; i ++) {
      this.error[i](object);
    }
  }
  public next(object: T): Observable<T> {
    this.callSuccess(object);
    return this;
  }

  public reject(obj: any): Observable<T> {
    this.callError(obj);
    return this;
  }

  public getObservers(): {successes: Array<(resp: T) => void>, errors: Array<(resp: any) => void>} {
    return {
      successes: this.success,
      errors: this.error
    };
  }
}

export class Minesweeper {

  protected row = 5;
  protected column = 5;
  protected mineCounts = 4;
  protected minesTrack: {id: string, hasMine: boolean, flagged: boolean, opened: boolean}[][] = [];
  protected minesFlatMap: {id: string, hasMine: boolean,  flagged: boolean, opened: boolean}[] = [];
  protected table: HTMLTableElement;
  protected nodeList: HTMLElement[][] = [];
  protected gameOver = false;
  protected containerId = '';
  protected boxOpened = 0;

  protected mineAudioSource: string;
  protected boxOpenAudioSource: string;
  protected gameWinAudioSource: string;
  protected timeTickAudioSource: string;

  protected audioPlayer: HTMLAudioElement;

  protected time = 0;
  protected intervalRef: number;

  public gameObservable: Observable<{win: boolean, over: boolean}>;
  public timeObservable: Observable<number>;

  public constructor() {
    Object.defineProperty(this, 'gameObservable', {
      writable: false,
      value: new Observable()
    });
    Object.defineProperty(this, 'timeObservable', {
      writable: false,
      value: new Observable()
    });
  }

  protected getAudioPlayer(): HTMLAudioElement {
    if (!this.audioPlayer) {
      this.audioPlayer = new Audio();
    }
    return this.audioPlayer;
  }

  public setMineAudioSource(src: string): void {
    this.mineAudioSource = src;
  }

  public setBoxOpenAudioSource(src: string): void {
    this.boxOpenAudioSource = src;
  }

  public setWinAudioSource(src: string): void {
    this.gameWinAudioSource = src;
  }

  public setTimeTickAudioSource(src: string): void {
    this.timeTickAudioSource = src;
  }

  public setMinesCount(count: number): void {
    if (count >= this.row * this.column || count < 1) {
      throw Error('Mine counts cannot be equal to or greater than row * column or less than one');
    }
    const boxes = this.row * this.column;
    if (boxes > 80) {
      count = 35;
    } else if (boxes > 50  && boxes < 80) {
      count = 20;
    } else if (boxes > 40 && boxes < 50) {
      count = 16;
    } else {
      count = 14;
    }
    this.mineCounts = Math.floor(count);
  }

  public setRows(count: number): void {
    if (!count || count <= 5) {
      return;
    }
    this.row = Math.floor(count);
  }

  public setColumns(count: number): void {
    if (!count || count <= 5) {
      return;
    }
    this.column = Math.floor(count);
  }

  public playAudio(which: 'mine' | 'box' | 'win' | 'tick'): Promise<void> {
    this.getAudioPlayer();
    switch (which) {
      case 'box':
        if (this.boxOpenAudioSource) {
          this.audioPlayer.src = this.boxOpenAudioSource;
          return this.audioPlayer.play();
        }
        break;
      case 'mine':
        if (this.mineAudioSource) {
          this.audioPlayer.src = this.mineAudioSource;
          return this.audioPlayer.play();
        }
        break;
      case 'win':
        if (this.gameWinAudioSource) {
          this.audioPlayer.src = this.gameWinAudioSource;
          return this.audioPlayer.play();
        }
        break;
      case 'tick':
        if (this.timeTickAudioSource) {
          this.audioPlayer.src = this.timeTickAudioSource;
        }
        break;
    }
  }

  /**
   * Get random number for given max value
   * @param max the maximum number
   * @private
   */
  private getRandomInt(max): number {
    return Math.floor(Math.random() * max);
  }

  private toggleClass(subject: string, str: string): string {
    if (subject.indexOf(str) > -1 ) {
      subject.replace(str, '');
    } else {
      subject += ' ' + str;
    }
    return subject;
  }

  private appendClass (subject: string, str: string): string {
    if (subject.indexOf(str) > -1 ) {
      return subject;
    }
    subject += ' ' + str;
    return subject;
  }

  private removeClass (subject: string, str: string): string {
    return subject.replace(str, '');
  }

  private getNeighborNodesPositions(row, column, maxRow, maxColumn, ignoreDiagonalData?: boolean): Array<Array<number>> {
    let ret = [];
    for (let i = row - 1; i <= (row + 1); i++) {
      for (let j = column - 1; j <= (column + 1); j++) {
        if (i < 0 || j < 0) { continue; }
        if (i > maxRow || j > maxColumn) { continue; }
        ret.push ([i, j]);
      }
    }
    if (ignoreDiagonalData) {
      ret[0] = null;
      ret[2] = null;
      ret[6] = null;
      ret[8] = null;
      ret = ret.filter((a) => {
        return a !== null;
      });
    }
    return ret;
  }

  private generateTableRows(): void {
    this.nodeList = [];
    this.minesTrack = [];
    this.minesFlatMap = [];
    for (let i = 0; i < this.row; i++) {
      this.minesTrack.push([]);
      this.nodeList.push([]);
      const tr = document.createElement('tr') as HTMLTableRowElement;
      for (let j = 0; j < this.column; j++) {
        const obj = {id: i + '' + j, hasMine: false, flagged: false, opened: false};
        this.minesTrack[i].push(obj);
        this.minesFlatMap.push(obj);
        const td = document.createElement('td') as HTMLElement;
        this.nodeList[i].push(td);
        tr.appendChild(td);
      }
      this.table.appendChild(tr);
    }
  }

  private putMines(): void {
    const ref = [];
    for (let i = 0; i < this.minesFlatMap.length; i ++) {
      ref.push(this.minesFlatMap[i]);
    }
    for (let i = 0; i < this.mineCounts; i ++) {
      const p = this.getRandomInt((ref.length) - 1);
      ref[p].hasMine = true;
      ref.splice(p, 1);
    }
  }

  private getMineCounts(i: number, j: number): number {
    let mCount = 0;
    const minePlace = this.minesTrack[i][j];
    if (!minePlace.hasMine) {
      const neighborPositions = this.getNeighborNodesPositions(i, j, this.row - 1, this.column - 1);
      for (let k = 0; k < neighborPositions.length; k ++) {
        const pos = neighborPositions[k];
        if (this.minesTrack[pos[0]][pos[1]].hasMine) {
          mCount ++;
        }
      }
      return mCount;
    }
    return null;
  }

  private openEmptyCellRecursive(i: number, j: number): void {
    if (this.minesTrack[i][j].opened) {
      return;
    }
    const mines = this.getMineCounts(i, j);
    this.nodeList[i][j].innerText = mines === 0 ? ' ' : (mines + '');
    this.nodeList[i][j].className = this.appendClass(this.nodeList[i][j].className, 'opened');
    this.minesTrack[i][j].opened = true;
    this.boxOpened += 1;
    const neighborPositions = this.getNeighborNodesPositions(i, j, this.row - 1, this.column - 1, true);
    for (let k = 0; k < neighborPositions.length; k ++ ) {
      const pos = neighborPositions[k];
      const count = this.getMineCounts(pos[0], pos[1]);
      if (count === 0) {
        this.openEmptyCellRecursive(pos[0], pos[1]);
      }
    }
  }

  private checkGameWin(): boolean {
    return this.boxOpened === this.row * this.column - this.mineCounts;
  }

  public startTimer(): void {
    this.time = 0;
    if (this.intervalRef) {
      clearInterval(this.intervalRef);
    }
    this.intervalRef = setInterval(() => {
      this.time += 1;
      this.timeObservable.next(this.time);
    }, 1000);
  }

  private setEventListenerToMinePlace(): void {
    for (let i = 0; i < this.row; i ++) {
      for (let j = 0; j < this.column; j ++) {
        const node = this.nodeList[i][j] as HTMLElement;
        node.addEventListener('click', (e) => {
          if (this.gameOver) { return; }
          if (this.minesTrack[i][j].flagged) {
            return;
          }
          if (this.minesTrack[i][j].hasMine) {
            this.minesTrack[i][j].opened = true;
            this.nodeList[i][j].innerHTML = '&#x1f4a3;';
            this.gameOver = true;
            const promise = this.playAudio('mine');
            if (promise) {
              promise.then(() => {
                this.gameObservable.next({win: false, over: true});
              });
            } else {
              this.gameObservable.next({win: false, over: true});
            }
            clearInterval(this.intervalRef);
          } else {
            this.openEmptyCellRecursive(i, j);
            this.playAudio('box');
          }
          if (this.checkGameWin()) {
            this.gameOver = true;
            this.gameObservable.next({win: true, over: true});
            this.playAudio('win');
            clearInterval(this.intervalRef);
          }
        });
        node.addEventListener('contextmenu', (e) => {
          e.preventDefault();
          e.stopPropagation();
          if (this.gameOver) { return; }
          if ((this.minesTrack[i][j].opened && this.minesTrack[i][j].hasMine) || this.minesTrack[i][j].opened) {
            return;
          }
          this.minesTrack[i][j].flagged = !this.minesTrack[i][j].flagged;
          if (this.minesTrack[i][j].flagged) {
            this.nodeList[i][j].className = this.appendClass(this.nodeList[i][j].className, 'flagged');
          } else {
            this.nodeList[i][j].className = this.removeClass(this.nodeList[i][j].className, 'flagged');
          }
        });
      }
    }
  }

  public init(containerId: string, row?: number, column?: number, mines?: number): void {
    this.containerId = containerId;
    this.setRows(row);
    this.setColumns(column);
    this.setMinesCount(mines);
    const container = document.getElementById(containerId);
    this.table = document.createElement('table') as HTMLTableElement;
    this.table.cellSpacing = 0 + '';
    this.table.cellPadding = 0 + '';
    container.appendChild(this.table);
    this.generateTableRows();
  }

  public start(): void {
    this.table.innerHTML = '';
    this.generateTableRows();
    this.putMines();
    this.setEventListenerToMinePlace();
    this.startTimer();
  }

  public restart(): void {
    this.boxOpened = 0;
    this.gameOver = false;
    this.start();
  }

}
