import { Component, OnInit } from '@angular/core';
import { FxRatesService } from '../fx-rates.service';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog, MatSnackBar} from '@angular/material';
import {BuyConfirmationComponent} from '../components/buy-confirmation/buy-confirmation.component';
import {TradeInterface} from '../interfaces/trade.interface';
import {SnackbarService} from '../snackbar.service';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {

  public formGroup: FormGroup;

  public currentPrice: number;
  constructor(
    private fxRates: FxRatesService,
    protected snackbar: SnackbarService,
    protected dialog: MatDialog,
  ) {}

  protected initFormGroup(): void {
    const buyFormControl = new FormControl();
    const sellFormControl = new FormControl();
    const rateFormControl = new FormControl();
    this.formGroup = new FormGroup({
      buyAmount: buyFormControl,
      sellAmount: sellFormControl,
      rate: rateFormControl,
    });
    buyFormControl.valueChanges.subscribe((value) => {
      rateFormControl.setValue(this.currentPrice);
      value = (value * this.currentPrice).toFixed(4);
      if (value == 0) {
        value = '';
      }
      sellFormControl.setValue(value, {
        emitEvent: false,
      });
    });

    sellFormControl.valueChanges.subscribe((value) => {
      rateFormControl.setValue(this.currentPrice);
      value = (value * this.currentPrice).toFixed(4);
      if (value == 0) {
        value  = '';
      }
      buyFormControl.setValue(value , {
        emitEvent: false,
      });
    });

  }

  ngOnInit(): void {
    this.initFormGroup();
    this.fxRates.getFeed().subscribe(
      response => {
        this.currentPrice = response;
      },
      error => {
        alert('Error fetching rate feed.');
        console.warn('Error fetching rate feed.', error);
      }
    );

  }

  public submit (): void {
    if (!this.formGroup.value.buyAmount) {
      this.snackbar.show('Please enter either buying amount or selling amount.');
      return;
    }
    const data: TradeInterface = this.formGroup.value;
    if (data.sellAmount <= 0 || data.buyAmount <= 0) {
      this.snackbar.show('Please enter a valid buying or selling amount.');
      return;
    }
    const dialog = this.dialog.open(BuyConfirmationComponent, {data});
    dialog.afterClosed().subscribe(success => {
      if (success) {
        this.formGroup.reset();
      }
    });
  }

}
