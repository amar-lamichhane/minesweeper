import {Component, OnInit} from '@angular/core';
import {Minesweeper} from './minesweeper/minesweeper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private minesweeper: Minesweeper;
  public time: number;
  public ngOnInit(): void {
    this.minesweeper = new Minesweeper();
    this.minesweeper.init('game-container');
    this.minesweeper.setMineAudioSource('assets/audio/bomb.mp3');
    this.minesweeper.setBoxOpenAudioSource('assets/audio/box-open.wav');
    this.minesweeper.setWinAudioSource('assets/audio/win.wav');
    this.minesweeper.setTimeTickAudioSource('asset/audio/tick.wav');
    this.minesweeper.gameObservable.subscribe(d => {
    });
    this.minesweeper.timeObservable.subscribe(t => this.time = t );
  }

  public restartGame(): void {
    this.minesweeper.restart();
  }
}
