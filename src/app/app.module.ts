import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { IntroComponent } from './intro/intro.component';
import { TradeComponent } from './trade/trade.component';
import { FxRatesService } from './fx-rates.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatInputModule,
  MatListModule, MatProgressSpinnerModule,
  MatSnackBarModule
} from '@angular/material';
import {NeoLogoComponent} from './components/neo-log-component/neo-logo.component';
import {BuyConfirmationComponent} from './components/buy-confirmation/buy-confirmation.component';
import {BuyConfirmationService} from './buy-confirmation.service';
import {SnackbarService} from './snackbar.service';

const appRoutes: Routes = [
  { path: '',      component: IntroComponent },
  { path: 'trade', component: TradeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TradeComponent,
    IntroComponent,
    NeoLogoComponent,
    BuyConfirmationComponent,
  ],
  entryComponents: [BuyConfirmationComponent],
    imports: [
        BrowserModule,
        RouterModule.forRoot(
            appRoutes
        ),
        FormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatListModule,
        MatCardModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
    ],
  providers: [FxRatesService, BuyConfirmationService, SnackbarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
