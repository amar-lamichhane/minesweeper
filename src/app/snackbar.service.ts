import { Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class SnackbarService {
  constructor(protected snackbar: MatSnackBar) {
  }
  public show(message: string, duration: number = 3000): void {
    this.snackbar.open(message, null, {
      duration: duration,
      verticalPosition: 'top'
    });
  }
}
