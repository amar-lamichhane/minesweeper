import {Injectable} from '@angular/core';
import {TradeInterface} from './interfaces/trade.interface';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class BuyConfirmationService {
  /**
   * Post the data to server for confirmed trade
   * @param trade
   */
  public buy(trade: TradeInterface): Observable<boolean> {
    return Observable.create(observe => {
      setTimeout(() => {
        observe.next(true);
      }, 1000);
    });
  }
}
