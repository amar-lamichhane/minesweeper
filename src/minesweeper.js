/*
<table id="table" >

</table>

tr {
  margin: 0;padding: 0;
}
td {
  padding: 0;
  margin: 0;
  width: 20px;
  height: 20px;
  border: 1px solid #ccc;
}

 */

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}
function getNeighborNodesPositions(row, column, maxRow, maxColumn) {
  const ret = [];
  for (let i=row-1; i <= (row + 1); i++) {
    for (let j= column-1; j <= (column+1); j++) {
      if (i < 0 || j < 0) continue;
      if (i > maxRow || j > maxColumn) continue;
      ret.push ([i,j]);
    }
  }
  return ret;
}

let table = document.getElementById("table");
const row = 10;
const column = 10;
const minesCount = 20;
// attach row columns
let td = "";
for (let i=0; i < row; i++) {
  td +="<tr>";
  for (let j=0; j < column; j++) {
    td += "<td id='box-"+i+'' +j+"'></td>"
  }
  td +="</tr>"
}

table.innerHTML = td;
const nodeList = [];
const tds = table.querySelectorAll('td');
for(let i=0; i <tds.length; i ++) {
  nodeList.push (tds[i]);
}
// randomly pick mine-counts nodes
for (let i=0; i<minesCount; i++) {
  const minePlace = getRandomInt(nodeList.length);
  tds[minePlace].innerText = '*';
  //remove from the tds list
  nodeList.splice(minePlace, 1);
}
let index = 0;
for (let i=0; i<row; i++) {
  for (let j=0; j<column; j++){
    let mCount = 0;
    console.log(index, tds[index]);
    if (tds[index].innerText !== '*') {
      const neighborPositions = getNeighborNodesPositions(i, j, row-1, column-1);
      for (k = 0; k < neighborPositions.length; k ++) {
        const pos = neighborPositions[k];
        console.log(pos);
        if (table.querySelector('#box-'+ pos[0]+''+pos[1]).innerText === '*') {
          mCount ++;
        }
      }
      tds[index].innerText = mCount;
    }
    index ++;
  }
}
